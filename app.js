const axios = require('axios')
const CommandoClient = require('./client')
const token = "OTU1NzQ1MzEwNTYxNDAyOTEw.YjmJLg.X9-zaUPbWtbwN_TMjgSLYQE9nOw"

const client = new CommandoClient()

setInterval(function () {
  axios.get('https://api.dexscreener.io/latest/dex/pairs/avalanche/0x9eaf7ca996e996b64bfe62e76ddcf69f87100820')
    .then(function (response) {
      const {priceChange, priceUsd} = response.data.pair

      if (priceChange.h24 > 0) {
        client.user.setActivity(`$${priceUsd} ↗ ${priceChange.h24}%`, {type: 'PLAYING'})
      } else {
        client.user.setActivity(`$${priceUsd} ↘ ${priceChange.h24}%`, {type: 'PLAYING'})
      }
    })
}, 60000)

client.login(token)
